import {configureStore} from '@reduxjs/toolkit';
import counter from '../features/counter/counterSlice';
import bakery from '../features/shops/bakery';
import weaponShop from '../features/shops/weaponShop';

export default configureStore({
    reducer: {
        counter,
        bakery,
        weaponShop,
    },
});
