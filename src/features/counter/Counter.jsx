import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {add} from './counterSlice';

export const Counter = () => {

    const count = useSelector(state => state.counter);
    const dispatch = useDispatch();

    return (
        <div className="inline-block overflow-hidden rounded-xl bg-gray-200">
            <button className="bg-blue-500 text-white px-2"
                    onClick={() => dispatch(add(-1)) }>-</button>
            <span className="px-2">{count}</span>
            <button className="bg-blue-500 text-white px-2"
                    onClick={() => dispatch({type: 'counter/add', payload: 2})}>+</button>
        </div>
    );
};
