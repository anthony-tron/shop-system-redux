import {createEntry, createShop} from './createShop';

export default createShop({
    name: "Weapon Shop",
    items: [
        createEntry('Repair powder', 8),
        createEntry('Gun repair powder', 16),
        createEntry('Classic gun', 15),
        createEntry('Battle wrench', 20),
        createEntry('Baselard', 25),
        createEntry('Gladius', 30),
    ],
}).reducer;