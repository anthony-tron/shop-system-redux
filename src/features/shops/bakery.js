import {createEntry, createShop} from './createShop';

export default createShop({
    name: "Polly's Bakery",
    items: [
        createEntry('Flour', 2),
        createEntry('Super hot pepper', 2),
        createEntry('Sugar cane', 2),
        createEntry('Bread', 3),
        createEntry('Tasty water', 9),
    ],
}).reducer;
