import {useSelector, useDispatch} from 'react-redux';
import {add} from '../counter/counterSlice';


export const Shop = ({name}) => {

    const wallet = useSelector(state => state.counter);
    const shopName = useSelector(state => state[name].name);
    const items = useSelector(state => state[name].items);
    const dispatch = useDispatch();

    const buy = (id, price) => {
        dispatch(add(-price));
        dispatch({type: `${name}/take`, payload: id});
    };

    return (
        <div>
            <h2>Welcome to <em>{shopName}</em></h2>
            <p className="my-2">Look what I've got!</p>
            <ul>
                { items.map(({id, name, count, price}) =>
                    <li key={id} className="space-x-3">
                        <button className="rounded-xl px-2 bg-gray-200 text-gray-800 hover:text-white hover:bg-blue-500 disabled:opacity-50 disabled:cursor-default"
                                disabled={count === 0 || wallet < price}
                                onClick={() => buy(id, price)} >$ {price}</button>
                        <span className="font-light">{name}</span>
                        <span className="text-sm font-light">{count && <span> ×{count}</span>}</span>
                    </li>
                )}
            </ul>
        </div>
    );
};
