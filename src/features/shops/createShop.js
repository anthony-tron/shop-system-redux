import {createSlice, nanoid} from '@reduxjs/toolkit';

export const createShop = ({name, items = []}) => createSlice({
    name,
    initialState: {
        name,
        items,
    },
    reducers: {
        addItem: (state, {payload}) => {
            state.items.push(payload);
        },
        take: ({items}, {payload}) => {
            const item = items.find(e => e.id === payload);
            item.count && (item.count -= 1);
        },
    },
});

export const createEntry = (name, price, count) => ({
    id: nanoid(),
    name,
    price,
    count
});
