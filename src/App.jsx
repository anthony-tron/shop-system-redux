import {Counter} from './features/counter/Counter';
import {Shop} from './features/shops/Shop';

export const App = () => {

    return (
        <div className="min-h-screen bg-gray-50 md:bg-gray-200">
            <div className="container mx-auto px-8 py-6 bg-gray-50 prose lg:prose-lg">
                <h1>Demo</h1>
                <p>Click on <strong>-</strong> or <strong>+</strong> to decrease / increase your money.</p>
                <Counter />
                <Shop name='bakery' />
                <hr />
                <Shop name='weaponShop' />
            </div>
        </div>
    );
}